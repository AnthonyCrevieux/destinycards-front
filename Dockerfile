FROM nginx:alpine

WORKDIR /usr/share/nginx/html

COPY index.html /usr/share/nginx/html/index.html

COPY js/ /usr/share/nginx/html/js

COPY img/ /usr/share/nginx/html/img

COPY css/ /usr/share/nginx/html/css

COPY test.json /usr/share/nginx/html/test.json

COPY api.destinycards.local.conf /usr/share/nginx/html/api.destinycards.local.conf
